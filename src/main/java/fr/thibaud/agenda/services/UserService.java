package fr.thibaud.agenda.services;

import org.springframework.security.core.userdetails.User;

import java.util.List;
import java.util.Optional;

public class UserService {
    User saveUser(User user);

    boolean existsById(Long id);

    List<User> getUsers();

    void deleteById(Long id);

    Optional<User> findById(Long id) {
    }

}
