package fr.thibaud.agenda.api.mappers;

import fr.thibaud.agenda.api.dto.user.UserCreateDTO;
import fr.thibaud.agenda.api.dto.user.UserDTO;
import fr.thibaud.agenda.models.user.Users;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    Users fromDto(UserDTO userDTO);
    List<Users> fromDtos(List<UserDTO> userDto);
    UserDTO toDto(Users user);
    List<UserDTO> toDtos(List<Users> users);
}