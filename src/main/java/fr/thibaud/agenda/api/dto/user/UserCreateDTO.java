package fr.thibaud.agenda.api.dto.user;

import fr.thibaud.agenda.models.user.Roles;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDTO {
    private String login;
    public String password;
    private Roles role;
}
