package fr.thibaud.agenda.api.dto.user;

import fr.thibaud.agenda.models.user.Roles;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDTO {
    private Long id;
    private String login;
    public String password;
    private Roles role;
}
