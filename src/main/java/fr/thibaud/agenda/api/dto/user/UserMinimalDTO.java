package fr.thibaud.agenda.api.dto.user;


import fr.thibaud.agenda.models.user.Roles;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UserMinimalDTO {
    private Long id;
    private String login;
    private Roles role;
}
