package fr.thibaud.agenda.controllers;

import fr.thibaud.agenda.api.dto.user.UserCreateDTO;
import fr.thibaud.agenda.api.dto.user.UserDTO;
import fr.thibaud.agenda.api.mappers.UserMapper;
import fr.thibaud.agenda.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getAll(
    ) {
        return ResponseEntity.ok(this.userMapper.toDtos(this.userService.findAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getById(
            @PathVariable() Long id
    ) {
        return ResponseEntity.of(
                this.userService.findById(id).map(this.userMapper::toDto)
        );
    }

    @PostMapping()
    public ResponseEntity<UserDTO> createUser(
            @RequestBody UserCreateDTO userCreateDTO
    ) {
        User createdUser = userService.saveUser(
                this.userMapper.fromDto(userCreateDTO)
        );

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(this.userMapper.toDto(createdUser));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> updateUser(
            @PathVariable() Long id,
            @RequestBody UserDTO userDTO
    ) {
        if (!id.equals(userDTO.getId())) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .build();
        }

        if (this.userService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        ;

        User updatedUser = userService.saveUser(
                this.userMapper.fromDto(userDTO)
        );
        return ResponseEntity
                .ok(this.userMapper.toDto(updatedUser));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(
            @PathVariable() Long id
    ) {
        if (!this.userService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        ;

        this.userService.deleteById(id);

        if (this.userService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }

        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build();
    }


}
