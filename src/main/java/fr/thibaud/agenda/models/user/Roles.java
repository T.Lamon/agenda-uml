package fr.thibaud.agenda.models.user;

public enum Roles {
    USER,
    ADMIN
}
