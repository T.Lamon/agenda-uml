package fr.thibaud.agenda.models;

import jakarta.persistence.*;
import lombok.*;

@Data
@Builder
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contact")
public class Emails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "email", nullable = false, unique = true)
    protected String email;

    @ManyToOne
    private Contacts contact;
}
