package fr.thibaud.agenda.models;

import jakarta.persistence.*;
import lombok.*;

@Data
@Builder
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contact")
public class Websites {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "website", nullable = false, unique = true)
    protected String website;

    @ManyToOne
    private Contacts contact;
}
