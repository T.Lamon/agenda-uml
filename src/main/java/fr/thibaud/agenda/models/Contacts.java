package fr.thibaud.agenda.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contact")
public class Contacts {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "login", nullable = false, unique = true, length = 100)
    private String login;

    @OneToMany
    private List<Addresses> address = new ArrayList<>();
    @OneToMany
    private List<Phones> phone = new ArrayList<>();
    @OneToMany
    private List<Websites> website = new ArrayList<>();
    @OneToMany
    private List<Emails> email = new ArrayList<>();
    
}
