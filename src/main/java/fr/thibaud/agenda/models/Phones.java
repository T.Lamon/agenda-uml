package fr.thibaud.agenda.models;

import jakarta.persistence.*;
import lombok.*;

@Data
@Builder
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contact")
public class Phones {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "phone", nullable = false, unique = true)
    protected String phone;

    @ManyToOne
    private Contacts contact;
}
