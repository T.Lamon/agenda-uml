package fr.thibaud.agenda.models;

import fr.thibaud.agenda.models.user.Users;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "agenda")
public class Agendas {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "label", nullable = false)
    private String label;

    @ManyToOne
    private Users user;

    @OneToMany
    private List<Contacts> contacts = new ArrayList<>();
}
