package fr.thibaud.agenda.models;

import jakarta.persistence.*;
import lombok.*;

@Data
@Builder
@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contact")
public class Addresses {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "address", nullable = false, unique = true, length = 100)
    protected String address;

    @ManyToOne
    private Contacts contact;
}
