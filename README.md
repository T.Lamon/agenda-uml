#Agenda-UML

Réalisé par : Thibaud LAMON

Pour lancer le programme :

1. Lancer Docker Desktop sur votre machine, puis lancer le script `docker-compose.yaml` dans le répertoire du projet.
2. Lancer le projet depuis le fichier `AgendaUmlLamonApplication` dans votre IDE.

Note : Le projet sera lancé sur le port 8080 de votre machine et la base sur le port 3306. veillez à ce que ces ports soient disponibles.